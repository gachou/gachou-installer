describe("login", () => {
  beforeEach(() => {
    cy.testSetup({
      testUsers: [{ username: "bob", password: "pw-bob" }],
    });
  });

  it("shows login page", () => {
    cy.visit("/");
    cy.contains("Please sign in");
  });

  it("access is granted on for the correct password, shows login page on logout", () => {
    cy.visit("/");
    cy.login("bob", "pw-bob");
    cy.contains("Homepage");
    cy.logout("bob");
    cy.contains("Please sign in");
  });

  it("access is denied on for the wrong password", () => {
    cy.visit("/");
    cy.login("bob", "wrong-password");
    cy.contains("Login failed");
  });
});
