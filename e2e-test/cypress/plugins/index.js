/// <reference types="cypress" />

module.exports = (on, config) => {
  console.log("loading plugin")
  if (process.env.API_BASE_URL != null) {
    config.env.API_BASE_URL = process.env.API_BASE_URL
    console.log("Overriding API_BASE_URL with " + config.env.API_BASE_URL)
  }
  return config
}
