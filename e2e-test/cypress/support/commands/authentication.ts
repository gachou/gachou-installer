/// <reference types="cypress" />

declare global {
    namespace Cypress {
        interface Chainable {
            login(username: string, password: string): void;
            logout(username: string): void;
        }
    }
}

Cypress.Commands.add('login', (username, password) => {
    Cypress.log({message: 'logging in '})
    cy.findByLabelText("Username").type(username)
    cy.findByLabelText("Password").type(password)
    cy.findByText("Login").click()
})

Cypress.Commands.add('logout', (username: string) => {
    Cypress.log({message: 'logging out'})
    cy.findByTestId("user-menu").within(() => {
        cy.findByRole("button").contains(username).click()
        cy.findByText('Logout').click()
    })
})

export {}
