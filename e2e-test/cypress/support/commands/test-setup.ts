/// <reference types="cypress" />

export type TestUserDto = {
    username: string;
    password: string;
};

export type TestSetupRequest = {
    testUsers: TestUserDto[];
};

declare global {
    namespace Cypress {
        interface Chainable {
            testSetup(setup: TestSetupRequest): void
        }
    }
}

Cypress.Commands.add('testSetup', (testSetup) => {
    cy.request({
        method: "POST",
        body: testSetup,
        url: `${Cypress.env("API_BASE_URL")}/devtest/reset-gachou-for-tests`,
        headers: {
            'X-Gachou-Test-Setup-Token': 'e2e-access-token'
        }
    });
})

export {}
