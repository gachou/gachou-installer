# Gachou-Installer


## Installers

### docker-compose

The basic way to install gachou is to use docker-compose.
An example is shown in the folder [gachou-docker-compose](gachou-docker-compose).

> Please be aware of the fact that every thing is every early work in progress.
> And that there are no features yet.

### AWS

The architectural goal of Gachou is, that contains of components that are 
available in typical cloud environments. For example, we want to use S3-compatible
file storage, so that it can be self-hosted (using [min.io](https://min.io/)) 
as well as deployed to AWS.

The backend is built with Quarkus, which may run in AWS Lambda environments, but
also in a local docker-environment.

In the future, this repository may contain terraform files for deploying Gachou to AWS 
or other cloud services.

## Running e2e-tests

The folder `e2e-test` contains a cypress installation with e2e-test. 



### Run e2e-tests against dev-environment

* Checkout `gachou-web-ui` and run `yarn dev` or `yarn dev:server` 
* Checkout `gachou-backend-2022` and run
  * `docker-compose up -d` 
  * `quarkus dev -Dgachou.dangerous-test-setup-access.token=e2e-access-token`
* Run `yarn cypress:open:dev`

When you are done...

* Run `docker-compose down` in the `gachou-backend-2022` directory

### Run e2e-tests against built containers

* Run `yarn update-app` to fetch the containers
* Run `yarn start-app`
* Run `yarn cypress:open`

When you are done...

* Run `yarn stop-app`